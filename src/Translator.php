<?php
/**
 * User: Jan Červený
 * Date: 20.1.14
 * Time: 7:42
 */

namespace Rampus;

use Nette\Localization\ITranslator;
use Nette\Localization\message;
use Nette\Localization\plural;

class Translator implements ITranslator {
    private $locale='cz';
    /**
     * Translates the given string.
     *
     * @param  string   message
     * @param  int      plural count
     * @return string
     */
    function translate($message, $count = NULL) {
        return $message;
    }

    public function setLocale($lang) {
                    $this->locale=strtolower($lang);
    }


} 