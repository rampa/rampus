<?php

    namespace Rampus;

    use Nette\Caching\Cache;
    use Nette\Database\Context;
    use Nette\Object;
    use Nette\Security\IAuthorizator;
    use Nette\Security\Permission;
    use Rampus\Norma\EntityManager;

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    class Authorizator extends Object implements IAuthorizator {
        const ALLOW_ALL = true;
        const DENY_ALL = false;

        /** @var Context */
        private $db;
        /** @var EntityManager  */
        private $em;

        /** @var Permission */
        private $acl;

        /** @var Cache */
        private $securityCache;
        private $mode = true;
        /** @var  Logger */
        private $logger;

        public function __construct(EntityManager $em, \Nette\Caching\Storages\FileStorage $cache) {
            $this->em = $em;
            //$this->logger=$logger;
            $this->securityCache = new Cache($cache, 'security');
        }

        function isAllowed($role, $resource, $privilege = null) {

            return $this->getAcl()->isAllowed($role, $resource, $privilege);
        }

        public function clearCache() {
            $this->securityCache->remove('acl');
            $this->acl=null;
        }

        public function setMode($mode) {
            $this->mode = $mode;
        }

        /**
         *
         * @return Permission
         */
        public function getACL() {
            $this->acl = $this->securityCache->load('acl', function () {
                $acl = new Permission();
                foreach ($this->em->getAll('UserRoles') as $rtmp) {
                    $acl->addRole($rtmp->name, $rtmp->parent?$rtmp->parent->name:null);
                }
                foreach ($this->em->getAll('Resource') as $rtmp) {
                    if (!$acl->hasResource($rtmp->resource))
                    $acl->addResource($rtmp->resource);
                }
                foreach ($this->em->getAll('ACL') as $atmp) {
                    if ($atmp->access) {
                        $acl->allow($atmp->role, $atmp->resource, $atmp->privilege);
                    } else {
                        $acl->deny($atmp->role, $atmp->resource, $atmp->privilege);
                    }
                }
                $acl->allow('admin');
                $this->securityCache->save('acl', $acl);
                return $acl;
            });
            return $this->acl;
        }
    }
