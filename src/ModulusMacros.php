<?php

    namespace Rampus;

/*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /**
     * Description of MRampusacros
     *
     * @author Jan
     */
    use Latte\MacroNode;
    use Latte\PhpWriter;

    class RampusMacros extends \Latte\Macros\MacroSet {

        private $a;

        public static function install(\Latte\Compiler $compiler) {
            $set = new static($compiler);
            $set->addMacro('sref',null,null, array($set, 'macroSec'));
        }

        public function startSec($node, $writer) {
            return $writer->write('<a>');
        }

        public function endSec($node, $writer) {
            return $writer->write('</a>');
        }

        /**
         * n:id="..."
         */
        public function macroSec(MacroNode $node, PhpWriter $writer) {
            dd($this);
                
            $node->htmlNode->attrs['id']="test";
            $a = \Nette\Utils\Html::el('a');
            $a->setText('pokus');
            //$node->openingCode='<a>';
            //dd($node);
            return $writer->write('echo %escape(%modify(' . ('$_presenter') . '->link(%node.word, %node.array?)))');
           //    return $writer->write(self::macroLink($node,$writer));
        }

    }
    