<?php
/**
 * Created by PhpStorm.
 * User: Jan
 * Date: 15.11.13
 * Time: 14:28
 */

namespace Rampus;

class Helpers {
    public static function makeConfirm($el, $message, $ajax = FALSE, $attr = array()) {
        $attr['data-confirm-text'] = $message;
        $attr['data-confirm'] = 'modal';
        dd($attr);
        if ($el instanceof \Nette\Utils\Html) {
            $el->addAttributes($attr);
            if ($ajax) {
                $el->addAttributes(['class' => 'dajax']);
            }
            return $el;
        }
        $res = '';
        foreach ($attr as $key => $val) {
            $res .= $key . '="' . $val . '" ';
        }
        return $res;
    }
    public static function bootstrapFormRender(\Nette\Application\UI\Form $form,$large=false){
        // setup form rendering
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = $large?'div class=col-sm-9':'div class=col-sm-4';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

// make form and controls compatible with Twitter Bootstrap
        $form->getElementPrototype()->class($form->getElementPrototype()->class.' form-horizontal');

        foreach ($form->getControls() as $control) {
            if ($control instanceof \Nette\Forms\Controls\Button) {
                $control->getControlPrototype()
                    ->addClass(empty($usedPrimary) ? 'btn btn-success' : 'btn btn-default');
                $usedPrimary = true;
            } elseif ($control instanceof \Nette\Forms\Controls\TextBase || $control instanceof \Nette\Forms\Controls\Checkbox || $control instanceof \Nette\Forms\Controls\MultiSelectBox) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif ($control instanceof \Nette\Forms\Controls\Checkbox || $control instanceof \Nette\Forms\Controls\CheckboxList || $control instanceof \Nette\Forms\Controls\RadioList) {
                $control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
            }
        }

    }
} 