<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    namespace Rampus;

    /**
     * Description of AcePresenter
     *
     * @author cerveny
     */
    class AcePresenter extends BaseSecuredPresenter {

        /**
         * @inject
         * @var \Nette\Database\Context
         */
        public $db;


        public function createComponentMainMenu() {
            $menu = new MenuTree\MenuTree();
            $menu->setSource($this->db->table('rampus_menus')->where('menu', 'admin')->order('sort'));
            return $menu;
        }
        public function createComponentModal(){
            return new \rampus\Modal\Modal();
        }

    }
