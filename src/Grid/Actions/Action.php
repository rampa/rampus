<?php
    /**
     * Author: Jan Červený
     * Email: cerveny@redsoft.cz
     * Date: 7.5.2015
     * Time: 15:18
     * Package: normaweb
     * Licence: proprietary
     */

    namespace Rampus\NGrid;


    use Nette\Application\UI\Presenter;
    use Nette\ComponentModel\Container;
    use Nette\ComponentModel\IContainer;
    use Nette\Utils\Callback;
    use Nette\Utils\Html;
    use Rampus\Norma\BaseProxy;

    class Action extends Container implements IAction
    {
        private $link;
        private $label;
        private $prototype;
        private $icon = "";
        private $renderer;
        private $ajax = false;

        public function __construct(IContainer $parent = null, $name = null, $link = null)
        {
            parent::__construct($parent, $name);
            $this->link = $link ? $link : $this->getName();
            $el = Html::el('a');
            $el->class = "btn btn-xs btn-minier btn-default";
            $el->setText($name);
            $this->prototype = $el;
            $this->renderer = function (BaseProxy $row, Html $prototype, Presenter $presenter) {
                $prototype->addAttributes(['href' => $presenter->link($this->link, $row['id'])]);

                return $prototype->__toString();
            };
        }

        /**
         * @return mixed
         */
        public function getLabel()
        {
            return $this->label;
        }

        /**
         * @param mixed $label
         * @return Action
         */
        public function setLabel($label)
        {
            $this->label = $label;

            return $this;
        }

        /**
         * @return mixed
         */
        public function getPrototype()
        {
            return $this->prototype;
        }


        /**
         * @return mixed
         */
        public function getLink()
        {
            return $this->link;
        }

        public function setIcon($txt)
        {
            $this->icon = $txt;
        }

        /**
         * @param mixed $link
         * @return Action
         */
        public function setLink($link)
        {
            $this->link = $link;

            return $this;
        }

        public function render($row, $presenter)
        {
            $el = Callback::invokeArgs($this->renderer, [$row, $this->getPrototype(), $presenter]);

            return $el;
        }

    }
