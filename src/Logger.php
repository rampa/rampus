<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    namespace Rampus;

    /**
     * Description of Logger
     *
     * @author cerveny
     */
    class Logger extends \Nette\Object {

        private $path;

        /** @var \Nette\Database\Context */
        private $db;
        private $table = 'rampus_log';

        /** @var \Nette\Security\User */
        private $user;
        private $levels = [1 => 'Critical', 'Error', 'Warning', 'Notice', 'Info'];

        public function __construct($path, \Nette\Database\Context $db, \Nette\Security\User $user) {
            $this->path = $path.'/../log';
            $this->db = $db;
            $this->user = $user;
        }

        public function getTable() {
            return $this->table;
        }

        public function setTable($table) {
            $this->table = $table;
            return $this;
        }

        public function logFile($message, $level = 4, $file = 'modulus.log', $modul = null) {
            if (isset($this->levels[$level])) {
                $level = $this->levels[$level];
            }
            $file = fopen($this->path . '/' . $file, 'a');
            $text = (new \Nette\Utils\DateTime())->format('d.m.Y H:n');
            $text.="\t" . $level . "\t" . $this->user->identity->fullname . "\t" . $message . "\n";
            fwrite($file, $text);
            fclose($file);
        }
            public function logDB($message, $level = 4,  $modul = null){
                $this->db->table($this->table)
                    ->insert([
                       'message'=>$message,
                        'user'=>$this->user->identity->fullname,
                        'level'=>isset($this->levels[$level])?$this->levels[$level]:$level,
                        'date'=>new \Nette\Utils\DateTime
                    ]);
            }

    }
    