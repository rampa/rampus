<?php
/**
 * User: Jan Červený
 * Date: 5.6.14
 * Time: 7:45
 */

namespace Rampus\Rampus;

use Nette\Application\UI\Control;
use Nette\ComponentModel\Component;

class BtnDropdown extends Control {
	private $actions = [];
	private $id;
	private $class='';
	private $icon;

	/**
	 * @param string $class
	 * @return Combo
	 */
	public function setClass($class) {
		$this->class.=' '. $class;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getClass() {
		return $this->class;
	}

	public function setIcon($icon) {
		$this->icon=$icon;
	}

	/**
	 * @return mixed
	 */
	public function getIcon() {
		return $this->icon;
	}

	public function addAction($name, $link=null,$param=null) {
		$act = new Action($name, $link,$param);
		$this->actions[] = $act;
		return $act;
	}

	/**
	 * @return string
	 */
	public function render() {
		$this->template->setFile(__DIR__ . '/BtnDropdown.latte');
		$this->template->id = $this->id;
		$this->template->actions = $this->actions;
		$this->template->render();

	}

	/**
	 * @param mixed $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	public function __toString() {
		$this->render();
		return '';
	}


}

class Action extends Component {
	private $label;
	private $link;
	private $param;
	/**
	 * @var array
	 */
	private $confirm=[];
	private $class=null;
	private $icon=null;

	function __construct($label, $link=null,$param=null) {
		$this->label = $label;
		$this->link = $link;
		$this->param=$param;
	}

	/**
	 * @param mixed $class
	 * @return Action
	 */
	public function setClass($class) {
		$this->class = $class;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getClass() {
		return $this->class;
	}


	/**
	 * @param mixed $label
	 * @return Action
	 */
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * @param mixed $link
	 * @return Action
	 */
	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLink() {
		return $this->link;

	}

	/**
	 * @param null $param
	 * @return Action
	 */
	public function setParam($param) {
		$this->param = $param;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getParam() {
		return $this->param;
	}

	/**

	 * @return Action
	 */
	public function setConfirm($message='Prosím potvrďte akci',$class='label-danger') {
		$this->confirm =['message'=>$message,'class'=>$class];
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getConfirm() {
		if($this->confirm){
			$res='data-confirm-text="'.$this->confirm['message'].'"';
			$res.=' data-confirm="modal"';
			$res.=' data-confirm-header-class="'.$this->confirm['class'].'"';
			return $res;
		}else{
			return false;
		}
	}

	/**
	 * @param null $icon
	 * @return Action
	 */
	public function setIcon($icon) {
		$this->icon = $icon;
		return $this;
	}

	/**
	 * @return null
	 */
	public function getIcon() {
		return $this->icon;
	}

}