<?php

namespace Rampus;

use Nette,
    Nette\Utils\Strings,
    Nette\Security\Passwords;

/**
 * Users management.
 */
class UserManager extends \Nette\Object implements \Nette\Security\IAuthenticator {

    const
            TABLE_NAME = 'users',
            COLUMN_ID = 'id',
            COLUMN_NAME = 'email',
            COLUMN_PASSWORD_HASH = 'password',
            COLUMN_ROLE = 'role';

    /** @var Nette\Database\Context */
    private $em;
    private $authorizator;


    public function __construct(Norma\EntityManager $em, Nette\Security\IAuthorizator $authorizator)
    {
        $this->em = $em;
        $this->authorizator = $authorizator;

    }

    /**
     * Performs an authentication.
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials) {
        list($username, $password) = $credentials;

        $row = $this->em->getOne('User', [self::COLUMN_NAME => $username]);

        if (!$row->_isInStore()) {
            throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        }
        elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        }
        elseif(!$row->enabled){
            throw new Nette\Security\AuthenticationException('Account is disabled');
        }
        elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
            $row[self::COLUMN_PASSWORD_HASH] = Passwords::hash($password);
        }

        $arr = $row->toArray();
        unset($arr[self::COLUMN_PASSWORD_HASH]);

        return new Nette\Security\Identity($row[self::COLUMN_ID], explode(',', $row[self::COLUMN_ROLE]), $arr);
    }

    /**
     * Adds new user.
     * @param  string
     * @param  string
     * @return void
     */
    public function add($username, $password) {
        $this->database->table(self::TABLE_NAME)->insert(array(
            self::COLUMN_NAME          => $username,
            self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
        ));
    }

}
