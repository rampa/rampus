<?php
    /**
     * User: Jan Červený
     * Date: 11.11.2014
     * Time: 11:35
     */

    namespace rampus\Modal;

    use Nette\Application\UI\Control;

    class Modal extends Control {
        private $content = null;

        public function setContent($content,$title=" ") {


            $this->template->content = $content;
            $this->template->title=$title;
        }

        public function render() {
            $this->template->setFile(__DIR__.'/modal.latte');
            $this->template->render();
        }
    }