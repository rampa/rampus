<?php
/**
 * User: Jan Červený
 * Date: 20.1.14
 * Time: 7:49
 */

namespace Rampus;

use Nette\Object;

class Modulus extends Object {
    private $translator;

    public function getTranslator() {
        if (!$this->translator) {
            $this->translator = new Translator();
        }
        return $this->translator;
    }
} 