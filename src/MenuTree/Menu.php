<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    namespace Rampus\MenuTree;

    /**
     * Description of Menu
     *
     * @author cerveny
     */
    class MenuTree extends \Nette\Application\UI\Control {

        private $menu;
        private $ulClass;
        private $liClass;
        private $currentClass = "active";
        private $attributes = ['brSeparator' => ' > '];

        /**
         * 
         * @param mixed $source
         */
        public function setSource($source) {
            if (count($this->getComponents())) {
                return;
            }
            foreach ($source as $row) {
                $item = $this->getOrCreate($row->id, !(bool) $row->parent);
                $item->setLabel(isset($row->label) ? $row->label : null);
                $item->setLink(isset($row->link) ? $row->link : null);
                $item->setClass(isset($row->class) ? $row->class : null);
                $item->setBadge(isset($row->badge) ? $row->badge : null);
                $item->setIcon(isset($row->icon) ? $row->icon : null);
                $item->setParams($row->params);
                if (isset($row->parent) && $row->parent) {
                    if ($item->getParent() && $row->parent != $item->getParent()->getName()) {
                        $item->getParent()->removeComponent($item);
                    }
                    $parent = $this->getOrCreate($row->parent, true);
                    $parent->addComponent($item, $row->id);
                }
            }
        }

        public function render($source = null, $param = null) {
            $this->setSource($source);
            $this->template->setFile(__DIR__ . '/menutree.latte');
            $this->template->menu = $this->getComponents();
            $this->template->render();
        }

        public function renderBr($source = null, $param = null) {
            $this->setSource($source);
            $bread = $this->getBread();
            $this->template->setFile(__DIR__ . '/br.latte');
            $req = $this->getPresenter()->getRequest();
            $location = $req->getPresenterName() . ':' . $req->getParameters()['action'];
            $item = $this->findByLink($location);
            $res = [];
            while (!($item->getParent() instanceof \Nette\Application\UI\Presenter)) {
                array_unshift($res, $item);
                $item = $item->getParent();
            }
            $this->template->bread = $res;
            $this->template->render();
        }

        /**
         * 
         * @param type $name
         * @return MenuItem | boolean
         */
        private function getItemByName($name) {
            foreach ($this->getComponents(true) as $item) {
                if ($item->getName() == $name) {
                    return $item;
                }
            }
            return false;
        }

        /**
         * Get existing item or create new one
         * 
         * @param type $name
         * @return \Modulus\MenuTree\MenuItem
         */
        private function getOrCreate($name, $attach = false) {
            $item = $this->getItemByName($name);
            if (!$item) {
                $item = new MenuItem(null, $name);
                if ($attach) {
                    $this->addComponent($item, $name);
                }
            }
            return $item;
        }

        private function getBread() {
            
        }

        private function findByLink($link) {
            foreach ($this->getComponents(true) as $item) {
                if ($item->getLink() == $link) {
                    return $item;
                }
            }
            return false;
        }

    }
    