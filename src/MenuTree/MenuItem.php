<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    namespace Rampus\MenuTree;

    /**
     * Description of MenuItem
     *
     * @author cerveny
     */
    class MenuItem extends \Nette\ComponentModel\Container {

        private $label;
        private $link;
        private $icon;
        private $class;
        private $badge = null;
        private $params;

        function getBadge() {
            return $this->badge;
        }

        function setBadge($badge) {
            $this->badge = $badge;
            return $this;
        }

        public function getClass() {
            return $this->class;
        }

        public function setClass($class) {
            $this->class = $class;
            return $this;
        }

        public function getLabel() {
            return $this->label;
        }

        public function getLink() {
            return $this->link;
        }

        public function getIcon() {
            return $this->icon;
        }

        public function setLabel($label) {
            $this->label = $label;
            return $this;
        }

        public function setLink($link) {
            $this->link = $link;
            return $this;
        }

        public function setIcon($icon) {
            $this->icon = $icon;
            return $this;
        }

        /**
         * @return mixed
         */
        public function getParams() {
            return $this->params;
        }

        /**
         * @param mixed $params
         */
        public function setParams($params) {
            $this->params = $params;
            return $this;
        }


        /**
         * 
         * @return \Nette\Application\UI\Presenter
         */
        public function getPresenter() {
            return $this->getParent()->getPresenter();
        }

        public function isLinkCurrent() {
            foreach ($this->getComponents(true) as $item) {
                if ($item->isLinkCurrent()) {
                    return true;
                }
            }
            return $this->link ? $this->getPresenter()->isLinkCurrent($this->link) : false;
        }

    }
    